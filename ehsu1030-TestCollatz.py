#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_1(self):
        s = "5 50\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 50)

    def test_read_2(self):
        s = "450 550\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 450)
        self.assertEqual(j, 550)

    def test_read_3(self):
        s = "3 33\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 3)
        self.assertEqual(j, 33)
    

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(5, 50)
        self.assertEqual(v, 112)

    def test_eval_6(self):
        v = collatz_eval(450, 550)
        self.assertEqual(v, 142)

    def test_eval_7(self):
        v = collatz_eval(3, 33)
        self.assertEqual(v, 112)

    def test_eval_8(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_9(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)

    def test_eval_10(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)
    

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 5, 50, 112)
        self.assertEqual(w.getvalue(), "5 50 112\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 450, 550, 142)
        self.assertEqual(w.getvalue(), "450 550 142\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 3, 33, 112)
        self.assertEqual(w.getvalue(), "3 33 112\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_1(self):
        r = StringIO("5 50\n450 550\n3 33\n47 99\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "5 50 112\n450 550 142\n3 33 112\n47 99 119\n")

    def test_solve_2(self):
        r = StringIO("28 83\n422 475\n316 500\n119 419\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "28 83 116\n422 475 129\n316 500 144\n119 419 144\n")

    def test_solve_3(self):
        r = StringIO("332 939\n114 631\n37 235\n54 289\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "332 939 179\n114 631 144\n37 235 128\n54 289 128\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
